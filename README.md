# Colibri

## ¿Que es? / What Is?
Colibri es una app para educación temprana en idiomas originarios: Guarani, Quechua y Aimara.

Colibri is an app for early education in native languages: Guarani, Quechua and Aymara.

## ¿Como esta hecho? / Who is made?
Colibri esta escrito en HTML, CSS y JAVASCRIPT, y usa el framework Apache Cordova para encapsular la App y crear instaladores para Windows, GNU/Linux y Android.

Colibri is written in HTML, CSS and JAVASCRIPT, and uses the Apache Cordova framework to encapsulate the App and create installers for Windows, GNU/Linux and Android.

## Compilar / Build?
cordova platform add android

cordova platform add electron

cordova build android

cordova build electron

## Descargas / Downloads
- [Windows 8+](https://gitlab.com/jorgeclaros/colibri/-/raw/main/builds/colibri.windows.x64.rar?ref_type=heads&inline=false)
- [Ubuntu 22.04+](https://gitlab.com/jorgeclaros/colibri/-/raw/main/builds/colibri.linux.x64.tar.gz?ref_type=heads&inline=false)
- [Android 10+](https://gitlab.com/jorgeclaros/colibri/-/raw/main/builds/colibri.android.apk?ref_type=heads&inline=false)
